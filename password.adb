          -- **** Preprocesor ****

with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
procedure password is
    tries : Integer; -- Will contain user input
    
          -- **** password's procedure ****
begin

    Put_Line("Hello, world!");
    
          -- **** Introduction of loop ****
    for I in reverse 1 .. 5 loop
    
          Put ("You only have " & Integer'Image (I) & " left: ");
          Get (tries);
          Put_Line ("");
          
          if tries = 387 then
              Put_Line("No error. Congratulation Solfus.");
              exit;
          end if;
    end loop;
end password;
